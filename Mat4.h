#ifndef Mat4H
#define Mat4H
#include "Vec4.h"
struct NoMatrix
{
	NoMatrix(){}
};
struct Range
{
	Range(){}
};
class Mat4
{
	Vec4* val;
	int size;
public:
	Mat4 (int n = 4);
	Mat4 (int n, int m);
	Mat4(const Mat4& x);
	~Mat4();
	void Print();
	Vec4& operator[](int i);
	Mat4& operator=(const Mat4& x);
	Mat4& operator+=(const Mat4& x);
	Mat4 operator+(const Mat4& x);
	Mat4 operator*(float x);
	Mat4 operator-();
	Mat4 operator*(Mat4& y);
	Mat4& operator*=(Mat4& y);
	Vec4 GetCol(int i);
};
	//Mat4 operator*(float x, Mat4&);
	Mat4 operator-(Mat4& x, Mat4& y);
	Mat4& operator-=(Mat4& x,Mat4& y);
	//Mat4 operator*(Mat4& x, Mat4& y);
#endif