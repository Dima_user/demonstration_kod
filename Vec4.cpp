
//#include <complex>
 
#include "Vec4.h"
Vec4::Vec4(int n)
{
	size = n;
	val = new float[size];
	for (int i = 0; i < size; i++)
	{
		val[i] = 1;
	}
}
Vec4::Vec4(float* ptr , int nn )
{
	val = new float[size = nn];
	for (int i = 0 ; i < size; i++)
	{
		*(val + i) = *(ptr + i);
	}
}

Vec4::Vec4(float a, float b)
{
	val = new float [size = 2];
	val[0] = a; val[1] = b;
}
Vec4::Vec4(float a, float b,float c )
{
	val = new float [size = 3];
	val[0] = a; val[1] = b; val[2] = c;
}
Vec4::Vec4(float a, float b, float c, float d)
{
	val = new float [size = 4];
	val[0] = a; val[1] = b; val[2] = c; val[3] = d;
}

Vec4::Vec4(Vec4& vec)
{
	val = new float [size = vec.size];
	for (int i = 0; i < size ; i++)
	{
		*(val + i) = *(vec.val + i);
	}
}
void Vec4::print()
{
	for (int i = 0; i < size; i++)
	{
		cout << *(val + i) << " ";
	}
}
Vec4& Vec4::operator =(const Vec4& vec)
{
	if (size != vec.size) 
	{
		delete[] val; 
		val = new float [size = vec.size];
	}
	for (int i = 0; i < size ; i++)
	{
		*(val + i) = *(vec.val + i);
	}
	return *this;
}


Vec4& Vec4::operator+=(Vec4& x)
{
	try
	{
		if (size != x.size)
		{
			char* str = "Ошибка: несогласованные размеры векторов"; 
			throw (str);	
		}
		for(int i = 0; i < size; i++)
			{
			val[i] += x.val[i];
			}
	}
	catch(char* err )
		{
			cout <<err;		
		}
	return *this;
}

Vec4 operator+(Vec4& x,Vec4& y)
{
	Vec4 sum;
	sum = x;
	sum+=y;
	return Vec4(sum);
}

Vec4 Vec4::operator-() 
{
	Vec4 tmp;
	tmp = *this;
	for(int i = 0; i < size; i++)
	{
		tmp.val[i] = -(tmp.val[i]);
	}
	return tmp;
}
Vec4& operator-=(Vec4& x,const Vec4& y)
{
	Vec4 tmp;
	tmp = y;
	tmp= -tmp;
	x = x+tmp;
	return x; 
}
Vec4 operator-(const Vec4& x, const Vec4& y)
{
	Vec4 dif; 
	dif = x; 
	dif-=y;
	return dif;
}
double Vec4::operator*(const Vec4& x)
{
	Vec4 mul; 
	mul = *this;
	double result = 0;
	try
	{
		if (mul.size != x.size) throw "Ошибка: векторы должны быть одинакового размера\n";
		for (int i = 0; i < mul.size; i++)
		{
			mul.val[i] = mul.val[i] * x.val[i];
			result+=mul.val[i];
		}
	}
	catch (char* err)	
	{
		cerr<<err;
	}
	return result; 
}




Vec4 Vec4::operator*(float x)
{
	Vec4 v = *this;
	for (int i = 0; i < size; i++)
	{
		v.val[i] = x*val[i];
	}
	return v;
}
Vec4 operator*(float x, Vec4 vect)
{
	return vect*x;
}
Vec4 Vec4::operator/(float x)
{
	Vec4 tmp = *this; 
	return tmp*(1/x);
}
Vec4& Vec4::operator/=(float x)
{
	Vec4& tmp = *this;
	tmp = tmp/x;
	return tmp;
}
Vec4 Only1()
{
	Vec4 x(1,1,1,1);
	return x;
}
float& Vec4::operator[](int i)
{
	float e = 1.0F;
	float& link = e;
	try
	{
		if (i < 0 || i >= size )	throw "Ошибка: выход за пределы вектора\n";
		return val[i];
	}
	catch (char* err)
	{
		cerr << err;
	}
	return link;
}
