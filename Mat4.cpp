#include "Mat4.h"

Mat4::Mat4(int n)
{
	Mat4::size = n;
	Mat4::val = new Vec4[n];
	for (int i = 0; i < size; i++)
	{
		val[i] = Vec4(n);
	}
}
Mat4::Mat4(int n, int m)
{
	Mat4::size = n;
	Mat4::val = new Vec4[n];
	for (int i = 0; i < size; i++)
	{
		val[i] = Vec4(m);
	}
}
void Mat4::Print()
{
	 for (int i = 0; i < Mat4::size; i++)
	{
		val[i].print(); cout << "\n";
	}
}
Vec4& Mat4::operator[](int i)
{
	return val[i];
}
Mat4::~Mat4()
{
	for (int i = 0; i < size; i++)
	{
		val[i].~Vec4();
	}
}
Mat4& Mat4:: operator=(const Mat4& x)
{
	if (size != x.size)
	{
		(*this).~Mat4();
		val = new Vec4[size = x.size];
	}
	for (int i = 0; i < size; i++ )
	{
		val[i] = x.val[i];
	}
	return *this;
}

Mat4::Mat4(const Mat4& x)
{
	val = new Vec4[size = x.size];
	for (int i = 0; i < size; i++ )
	{
		val[i] = x.val[i];
	}
}
Mat4& Mat4::operator+=(const Mat4& x)
{
	try
	{
		if (size != x.size)
		{
			throw ("количество строк матриц должно быть равно\n");
		}
		for (int i = 0; i < size; i++)
		{
			val[i]+= x.val[i];
		}
	}
	catch (char* err)
	{
		cerr << err;
	}
	return *this;
}
Mat4 Mat4::operator+(const Mat4& x)
{
	Mat4 sum;
	sum = *this;
	sum+=x;
	return Mat4(sum);
}
Mat4 Mat4:: operator*(float x)
{
	Mat4 tmp = *this;
	for (int i = 0; i < size; i++)
	{
		tmp.val[i] = x * val[i] ;
	}
	return tmp;
}
Mat4 operator*(float x, Mat4& matr)
{
	return matr.operator*(x);
}
Mat4 Mat4::operator-()
{
	Mat4 tmp = *this;
	tmp = (-1)*tmp;
	return tmp;
}

Mat4 operator-( Mat4& x, Mat4& y)
{
	return x + (-y);
}
Mat4& operator-=(Mat4& x, Mat4& y)
{
	x = x - y;
	return x; 
}
Mat4 Mat4::operator*(Mat4& y)
{
	Mat4 mul(size, y[0].Size()); 
	try
	{
		for(int i = 1; i < size; i++)
		{
			if (val[0].Size() != val[i].Size())
				throw NoMatrix();
		}
		for(int i = 1; i < y.size; i++)
		{
			if (y[0].Size() != y[i].Size())
				throw NoMatrix();
		}
		if (val[0].Size() != y.size)  // если матрицы перемножить невозможно
		{
			throw Range();
		}	
		for (int i = 0; i < mul.size; i++)
		{
			for(int j = 0; j < mul[0].Size();j++)
			{
				float sum = 0;
				for (int k = 0; k < val[0].Size(); k++)
				{
					sum+= val[i][k]*y[k][j];
				}
				mul[i][j] = sum;
			}
		}
	}
	catch (NoMatrix)
	{
		cerr << "Это не матрица:\размеры строк в матрице должны быть одинаковые\n";
	}
	catch (Range)
	{
		cerr << "Эти матрицы нельз¤ перемножать\n";
	}
	return mul;
}
Vec4 Mat4::GetCol(int j)
{
	try
	{
		if(j < 0 || j > val[0].Size()) throw ("ошибка: выход за пределы строк матрицы\n");
			Vec4 x = Vec4(size);
		for (int i = 0; i < x.Size(); i++)
		{
			x[i] = val[i][j];
		}
		return x;
	}
	catch(char* err)
	{
		cerr << err;
	}
}
Mat4& Mat4::operator*=(Mat4& y)
{
	int N = y[0].Size();
	Mat4 tmp(*this);
	
	if (val[0].Size() !=  y[0].Size())
	{
		for (int i = 0; i < size; i++)
			val[i] = Vec4(N);
	}

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < y[0].Size(); j++)
		{
			val[i][j] = tmp[i] * y.GetCol(j); 
		}
	}
	return *this;
}