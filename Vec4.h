#ifndef Vec4H
#define Vec4H

#include <iostream>
//#include <limits>
#include <locale>
//#include <string>
//#include <vector>
//#include <iterator>
#include <cstdlib>
//#include <map>
#include <cctype>

using namespace std;
class Vec4 {
	float* val;
	int size;
public:
	Vec4(int n = 4);
	Vec4(float* arr,int nn = 4);
	Vec4(float a, float b);
	Vec4(float a, float b,float c);
	Vec4(float a, float b, float c, float d);
    Vec4(Vec4&);
	~Vec4 () {delete[] val;}
	Vec4& operator=(const Vec4&);
	void print();
	int Size(){return size;}
	Vec4& operator+=(Vec4&);
	Vec4 operator-(); // унарный минус
	//Vec4& operator*=(const Vec4&);
	double operator*(const Vec4& x);
	Vec4 operator*(float x);
	Vec4 operator/(float x);
	Vec4& operator/=(float x);
	float& operator[](int i);
	
	//Vec4& operator-=(Vec4&); 
	//friend Vec4 operator+(Vec4&, Vec4&); 
	
};
Vec4 operator*(float x, Vec4 vect);
#endif 